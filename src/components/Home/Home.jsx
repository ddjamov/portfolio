import { Center, Box, Text } from "@chakra-ui/react";

const Home = () => {
  return (
    <>
      <Center>
        <Box>
          <Text>Zdorova, svet</Text>
        </Box>
      </Center>
    </>
  );
};

export default Home;
